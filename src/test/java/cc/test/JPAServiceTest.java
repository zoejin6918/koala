package cc.test;

import static org.junit.Assert.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.junit.Test;

import cc.bean.Transaction;
import junit.framework.Assert;

public class JPAServiceTest
{

    @Test
    public void typeAnnotations ()
    {
        // assert
        AssertAnnotations.assertType (Transaction.class, Entity.class, Table.class, NamedQueries.class);
    }
    
    @Test
    public void fieldAnnotations() {
      // assert
      AssertAnnotations.assertField(Transaction.class, "id", Id.class, Column.class);
      AssertAnnotations.assertField(Transaction.class, "time", Column.class); 
      AssertAnnotations.assertField(Transaction.class, "price", Column.class);
      AssertAnnotations.assertField(Transaction.class, "size", Column.class);
      AssertAnnotations.assertField(Transaction.class, "ticker", Column.class);
      AssertAnnotations.assertField(Transaction.class, "action", Column.class);
    }
  
    @Test
    public void entity() {
      // setup
      Entity a
      = ReflectTool.getClassAnnotation(Transaction.class, Entity.class);
      // assert
      assertEquals("", a.name());
    }

    @Test
    public void table() {
      // setup
      Table t
      = ReflectTool.getClassAnnotation(Transaction.class, Table.class);
      // assert
      assertEquals("Transaction", t.name());
    }

}

package cc.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import cc.bean.OrderResultMessage;
import cc.enums.Company;
import cc.jms.TradingMessageHandler;

public class JmsServiceTest
{
    private TradingMessageHandler tmh;
    
    @Before
    public void initial() {
        tmh = new TradingMessageHandler ();
    }
    
    @Test
    public void testMessage() throws InterruptedException {
        String st = "<trade>\r\n" + 
            "  <buy>true</buy>\r\n" + 
            "  <id>23333</id>\r\n" + 
            "  <price>107.604</price>\r\n" + 
            "  <size>20000</size>\r\n" + 
            "  <stock>AAPL</stock>\r\n" + 
            "  <whenAsDate>2018-07-31T11:33:22.801-04:00</whenAsDate>\r\n" + 
            "</trade>";
         tmh.run (st);
         
         Thread.sleep (15000);
         
         assertEquals (23333, OrderResultMessage.getId ());
         assertEquals(Company.AAPL, OrderResultMessage.getStock ());
         assertEquals (20000, OrderResultMessage.getSize ());
         assertNotNull (OrderResultMessage.getResponse ());
    }

}

package cc.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import cc.bean.MACDStrategy;
import cc.bean.MovingAverage;
import cc.bean.Stock;
import cc.enums.Action;
import cc.enums.Company;

public class MACDStrategyTest
{
    private MACDStrategy twoMA;
    private MovingAverage ma;
    private Stock ticker;
    
    @Before
    public void initial() {
        ticker = new Stock(Company.AAPL);
        twoMA = new MACDStrategy (ticker, 10, 4);
        ma = new MovingAverage (30);
    }
    
    @Test
    public void testMovingAverageInSize() {
        double[] input1 = {2, 2, 2, 4, 4, 4};
        ma.addNumber(input1);
        double result = ma.getAvg();
        assertEquals(0, result, 0);
        
        double[] input2 = {1, 2, 3, 4, 5 ,6 ,7 , 8, 9, 10,
                            1, 2, 3, 4, 5 ,6 ,7 , 8, 9, 10,
                            1, 2, 3, 4, 5 ,6 ,7 , 8, 9, 10};
        ma.addNumber(input2);
        result = ma.getAvg();
        assertEquals(5.5, result, 0);
    }
    
    @Test
    public void testMovingAverageOverSize() {
        double[] input1 = {2, 2, 2, 4, 4, 4};
        ma.addNumber(input1);
        double[] input2 = {1, 2, 3, 4, 5 ,6 ,7 , 8, 9, 10,
                            1, 2, 3, 4, 5 ,6 ,7 , 8, 9, 10,
                            1, 2, 3, 4, 5 ,6 ,7 , 8, 9, 10};
        ma.addNumber(input2);
        double result = ma.getAvg();
        assertEquals(5.5, result, 0);
    }
    
    @Test
    public void testMACDStrategy() {
        assertEquals(Action.NONE, twoMA.getAction () );
        twoMA.newPriceArrive(11);
        twoMA.newPriceArrive(10);
        twoMA.newPriceArrive(11);
        twoMA.newPriceArrive(10);
        twoMA.newPriceArrive(11);
        twoMA.newPriceArrive(10);
        // Long-term avg is 0 due to lack of data.
        assertEquals(10.5, twoMA.getShortTermAvg(), 0);
        assertEquals(0, twoMA.getLongTermAvg(), 0);
        assertEquals(Action.NONE, twoMA.runTwoMAStrategy ());
        
        twoMA.newPriceArrive(11);
        twoMA.newPriceArrive(10);
        twoMA.newPriceArrive(9);
        twoMA.newPriceArrive(10);
        twoMA.newPriceArrive(11);
        // Short-line and long-line do not cross.
        assertEquals(10.0, twoMA.getShortTermAvg(), 0);
        assertEquals(10.3, twoMA.getLongTermAvg(), 0);
        assertEquals(Action.NONE, twoMA.runTwoMAStrategy ());
        
        // Short-line and long-line cross.
        twoMA.newPriceArrive(13);
        assertEquals(10.75, twoMA.getShortTermAvg(), 0);
        assertEquals(10.60, twoMA.getLongTermAvg(), 0);
        assertEquals(Action.BUY, twoMA.runTwoMAStrategy ());
        twoMA.newPriceArrive(14);
        
    }

}

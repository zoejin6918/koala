package cc.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import cc.bean.PriceInfo;
import cc.enums.Company;
import cc.trading.PriceService;

import org.junit.Before;
import org.junit.Test;

public class PriceServiceTest
{
    private ArrayList<PriceInfo> priceInfoList;
    private PriceInfo latestPrice;
    
    @Before
    public void initial() {
        priceInfoList = new ArrayList<PriceInfo>();
        latestPrice = new PriceInfo ();
    }
    
    @Test
    public void testGetPriceInfoList()
    {
        priceInfoList = PriceService.getTotalPriceInfo (Company.AAPL, 30);
        assertEquals(30, priceInfoList.size ());
    }
    
    @Test
    public void testGetLatestPrice() {
        latestPrice = PriceService.getLatestPrice (Company.AAPL);
        assertNotNull (latestPrice.getTime ());
        assertTrue (latestPrice.getClose () > 0);
        assertTrue (latestPrice.getVolume () > 0);
    }

}

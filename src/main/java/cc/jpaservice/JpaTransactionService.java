package cc.jpaservice;

import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import cc.bean.Transaction;

/**
 * This is JPA service class, used to connect to oracle database.
 * @author Lihua Shen
 *
 */
public class JpaTransactionService
{
    private static EntityManagerFactory emf = Persistence
            .createEntityManagerFactory ("pu");
    private static EntityManager em = emf.createEntityManager ();

    public static void addTransaction (Transaction t)
    {
        Integer maxId = (Integer) em.createNamedQuery("Transaction.getMaxID").getSingleResult();
        if(maxId==null) {maxId=0;}
        t.setId( maxId + 1 );
        
        //TODO: Reset strategyId

        em.getTransaction ().begin ();
        em.persist (t);
        em.getTransaction ().commit ();

    }

    public static List<Transaction> getTransactionHistory (int period)
    {
        List<Transaction> transactions = em.createNamedQuery ("Transaction.findAll").getResultList ();
        
        int size = transactions.size ();
        if(size < period) {
            return transactions;
        } else {
            return transactions.subList (size-period, size);
        }
    }
    
    /*public static void deleteTransactionHistory ()
    {

        EntityManagerFactory emf = Persistence
            .createEntityManagerFactory ("pu");
        EntityManager em = emf.createEntityManager ();

            List<Transaction> transactions = getTransactionHistory();
        
 

            em.getTransaction ().begin();

            Query q = em.createQuery ("DELETE FROM Transaction");
            q.executeUpdate ();
            for(Transaction transaction : transactions) {
                em.remove (transaction);
            }

            em.getTransaction ().commit();
        
    }   */
}

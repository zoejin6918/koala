package cc.trading;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cc.bean.PriceInfo;
import cc.enums.Company;

/**
 * This is class for price service.
 * 
 * @author Zoe Zou
 *
 */
public class PriceService
{
    public static final String PRICE_SERVICE_URL = "http://incanada1.conygre.com:9080/prices/";
    
    public PriceService() {
        
    }
    
    public static ArrayList<PriceInfo> getTotalPriceInfo(Company ticker, int period) {
        ArrayList<PriceInfo> result = new ArrayList<PriceInfo>();
        try
        {
            URL url = new URL(PRICE_SERVICE_URL + "/" + ticker.toString () + "?periods=" + period);
            URLConnection yc = url.openConnection ();
            BufferedReader in = new BufferedReader (
                new InputStreamReader (yc.getInputStream ()));
            String inputLine;
            in.readLine (); // ignore first line.
            while ((inputLine = in.readLine ()) != null)
            {
                String[] p = inputLine.split (",");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                Date time = sdf.parse (p[0]);
                double open = Double.parseDouble (p[1]);
                double high = Double.parseDouble (p[2]);
                double low = Double.parseDouble (p[3]);
                double close = Double.parseDouble (p[4]);
                int volume = Integer.parseInt (p[5]);
       
                PriceInfo priceInfo = new PriceInfo (time, open, high, low, close, volume);
                result.add (priceInfo);
            }
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
        return result;
    }
    
    public static PriceInfo getLatestPrice(Company ticker) {
        PriceInfo result = new PriceInfo ();
        try
        {
            URL url = new URL(PRICE_SERVICE_URL + "/" + ticker.toString () + "?periods=1");
            URLConnection yc = url.openConnection ();
            BufferedReader in = new BufferedReader (
                new InputStreamReader (yc.getInputStream ()));
            String lastLine;
            in.readLine (); // Ignore first line.
            while ((lastLine = in.readLine ()) != null)
            {
                String[] p = lastLine.split (",");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                Date time = sdf.parse (p[0]);
                double open = Double.parseDouble (p[1]);
                double high = Double.parseDouble (p[2]);
                double low = Double.parseDouble (p[3]);
                double close = Double.parseDouble (p[4]);
                int volume = Integer.parseInt (p[5]);
                result = new PriceInfo (time, open, high, low, close, volume);
            }
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return result;
    }
}

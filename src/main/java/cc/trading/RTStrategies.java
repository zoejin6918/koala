package cc.trading;

import java.util.ArrayList;

import cc.bean.Strategy;


/**
 * This is a collector for running strategies.
 * 
 * @author Zoe
 *
 */
public class RTStrategies
{
    private ArrayList<Strategy> selectedStrategies;

    public RTStrategies() {
        selectedStrategies = new ArrayList<Strategy>();
    }
    
    public void setSelectedStrategies(ArrayList<Strategy> list) {
        this.selectedStrategies = list;
    }
    
    public ArrayList<Strategy> getSelectedStrategies() {
        return this.selectedStrategies;
    }
    
    public void removeStrategies() {
        selectedStrategies = new ArrayList<Strategy> ();
    }
}

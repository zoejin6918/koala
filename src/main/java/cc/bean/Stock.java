package cc.bean;

import cc.enums.Action;
import cc.enums.Company;

/**
 * This is an object class for stock. Can calculate average cost and profit&loss
 * @author Zoe
 *
 */
public class Stock
{
    private Company ticker;
    private double price;
    private double avgCost;
    private double profitOrLoss;
    private int numOfShares;
    
    public Stock() {
         
    }
    
    public Stock(Company ticker) {
        super();
        setTicker(ticker);
        setPrice(0);
        setAvgCost(0);
        setProfitOrLose (0);
        setNumOfShares (0);
    }
    
    public Stock (Company ticker, double price, double avgCost,
        double profitOrLose, int numOfShares)
    {
        super ();
        setTicker(ticker);
        setPrice(price);
        setAvgCost(avgCost);
        setProfitOrLose(profitOrLose);
        setNumOfShares (numOfShares);
    }

    public double updatePL(double currentMarketPrice) {
        setProfitOrLose (currentMarketPrice / this.avgCost - 1);
        return this.getProfitOrLose ();
    }
    
    public double updateAvgCost(double newPrice, int newShares) {
        double newAvgCost = (this.avgCost * this.numOfShares + newPrice * newShares) / (this.numOfShares + newShares);
        setAvgCost (newAvgCost);
        return newAvgCost;
    }
    
    public void newTrade(int numOfShares, double newPrice, Action action) {
        if (action == Action.BUY) {
            buyShares (numOfShares, newPrice);
        } else if (action == Action.SELL) {
            sellShares (numOfShares, newPrice);
        }
    }
    
    public void buyShares(int buyShares, double newPrice) {
        if (buyShares >= 0) {
            setNumOfShares (this.numOfShares + buyShares);
            updateAvgCost (newPrice, buyShares);
            updatePL (newPrice);
        } else {
            throw new IllegalArgumentException ("Number of shares must be positive.");
        }
        
    }
    
    public void sellShares(int sellShares, double newPrice) {        
        if (sellShares >= 0) {
            if (this.numOfShares >= sellShares) {
                setNumOfShares (this.numOfShares - sellShares);
                updateAvgCost (newPrice, (sellShares * -1));
                updatePL (newPrice);
            } else {
                setNumOfShares (0);
                updatePL (newPrice); // Calculate P&L before set the avgCost to 0.
                setAvgCost (0);
            }
        } else {
            throw new IllegalArgumentException ("Number of shares must be positive.");
        }
        
    }
    
    public int getNumOfShares() {
        return this.numOfShares;
    }
    
    public void setNumOfShares(int numOfShares) {
        this.numOfShares = numOfShares;
    }
    
    
    
    public Company getTicker ()
    {
        return ticker;
    }

    public void setTicker (Company ticker)
    {
        this.ticker = ticker;
    }

    public double getPrice ()
    {
        return price;
    }

    public void setPrice (double price)
    {
        this.price = price;
    }

    public double getAvgCost ()
    {
        return avgCost;
    }

    public void setAvgCost (double avgCost)
    {
        this.avgCost = avgCost;
    }

    public double getProfitOrLose ()
    {
        return profitOrLoss;
    }

    public void setProfitOrLose (double profitOrLose)
    {
        this.profitOrLoss = profitOrLose;
    }
}

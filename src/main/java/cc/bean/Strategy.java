package cc.bean;

import cc.enums.Action;

/**
 * This is an parent object class for strategies.
 * @author Zoe Zou
 *
 */
public class Strategy
{
    protected Stock stock;
    protected Action action;
    
    public Strategy(Stock stock) {
        setStock(stock);
        setAction(Action.NONE);
    }
    
    public void setStock(Stock stock) {
        this.stock = stock;
    }
    
    public Stock getStock() {
        return this.stock;
    }
    
    public Action getAction() {
        return this.action;
    } 
    
    public void setAction(Action a) {
        this.action = a;
    }

}

package cc.bean;

import java.util.ArrayList;

/**
 * This is an object class for movingAverage.
 * @author Zoe Zou
 *
 */
public class MovingAverage
{
    private int term_length;
    private ArrayList<Double> data;
    private double avg;
    
    public MovingAverage(int length) {
        this.data = new ArrayList<Double>();
        this.term_length = length;
        this.avg = 0.0;
    }
    
    public void addNumber(double[] numbers) {
        for (double n : numbers) {
            if (data.size() >= getTermLength()) {
              data.remove(0) ; 
            } 
            data.add(n);
        }
        setAvg(calculateAvg());
    }
    
    public void addNumber(double currentPrice) {
         if (data.size() >= getTermLength()) {
             data.remove(0); 
          } 
          data.add(currentPrice);
          setAvg(calculateAvg());
    }
    
    public int getTermLength() {
        return this.term_length;
    }
    
    private double calculateAvg() {
        double total = 0;
        if (data.size() < getTermLength()) {
            return 0;
        }
        else {
            for (double d : data ) {
                total += d;
            }
            return total / data.size();
        }
    }
    
    public void setAvg(double avg) {
        this.avg = avg;
    }
    
    public double getAvg() {
        return this.avg;
    }
    
}

package cc.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cc.enums.BrokerResponse;
import cc.enums.Company;

/**
 * This is an object class for order result.
 * @author Zoe Zou
 *
 */
public class OrderResultMessage
{
    private static String receivedMsg;
    private static BrokerResponse response;
    
    private static boolean buyOrSell;
    private static int id;
    private static double price;
    private static int size;
    private static Company stock;
    private static Date whenAsDate;
    
    
    public OrderResultMessage() {
        
    }
    
    public OrderResultMessage(String rMsg) {
        receivedMsg = rMsg;
        setAttributesByReceivedMsg(receivedMsg);
    }
    
    public OrderResultMessage(BrokerResponse response, OrderMessage orderMsg) {
        setResponse (response);
        setBuyOrSell (orderMsg.isBuyOrSell ());
        setId(orderMsg.getId ());
        setPrice (orderMsg.getPrice ());
        setSize (orderMsg.getSize ());
        setStock (orderMsg.getStock ());
        setWhenAsDate (orderMsg.getWhenAsDate ());
    }
    
    private static void setAttributesByReceivedMsg(String rm) {
        String buySt = rm.substring (rm.indexOf ("<buy>") + 5, rm.indexOf ("</buy>"));
        String idSt = rm.substring (rm.indexOf ("<id>") + 4, rm.indexOf ("</id>"));
        String priceSt = rm.substring (rm.indexOf ("<price>") + 7, rm.indexOf ("</price>"));
        String resultSt = rm.substring (rm.indexOf ("<result>") + 8, rm.indexOf ("</result>"));
        String sizeSt = rm.substring (rm.indexOf ("<size>") + 6, rm.indexOf ("</size>"));
        String stockSt = rm.substring (rm.indexOf ("<stock>") + 7, rm.indexOf ("</stock>"));
        String whenAsDateSt = rm.substring (rm.indexOf ("<whenAsDate>") + 12, rm.indexOf ("</whenAsDate>"));
        
        setBuyOrSell (Boolean.parseBoolean (buySt));
        setId(Integer.parseInt (idSt));
        setPrice (Double.parseDouble (priceSt));
        setResponse (BrokerResponse.valueOf (resultSt));
        setSize(Integer.parseInt (sizeSt));
        setStock (Company.valueOf (stockSt));
        setWhenAsDate (whenAsDateSt);
    }

    
    public static BrokerResponse getResponse ()
    {
        return response;
    }

    public static void setResponse (BrokerResponse res)
    {
        response = res;
    }

    public static String getReceivedMsg ()
    {
        return receivedMsg;
    }

    public static void setReceivedMsg (String rMsg)
    {
        receivedMsg = rMsg;
    }

    public static boolean isBuyOrSell ()
    {
        return buyOrSell;
    }

    public static void setBuyOrSell (boolean bOrS)
    {
        buyOrSell = bOrS;
    }

    public static int getId ()
    {
        return id;
    }

    public static void setId (int newId)
    {
        id = newId;
    }

    public static double getPrice ()
    {
        return price;
    }

    public static void setPrice (double newPrice)
    {
        price = newPrice;
    }

    public static int getSize ()
    {
        return size;
    }

    public static void setSize (int newSize)
    {
        size = newSize;
    }

    public static Company getStock ()
    {
        return stock;
    }

    public static void setStock (Company newStock)
    {
        stock = newStock;
    }

    public static Date getWhenAsDate ()
    {
        return whenAsDate;
    }

    public static void setWhenAsDate (Date time)
    {
        whenAsDate = time;
    }
    
    public static void setWhenAsDate(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try
        {
            whenAsDate = sdf.parse (time);
        }
        catch (ParseException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public String toString ()
    {
        String result = "<trade>\r\n" + 
            "  <buy>" + isBuyOrSell () + "</buy>\r\n" + 
            "  <id>" + getId() + "</id>\r\n" + 
            "  <price>" + getPrice () + "</price>\r\n" + 
            "  <result>" + getResponse () + "</result>\r\n" + 
            "  <size>" + getSize() + "</size>\r\n" + 
            "  <stock>" + getStock () + "</stock>\r\n" + 
            "  <whenAsDate>" + getWhenAsDate () + "</whenAsDate>\r\n" + 
            "</trade>";
        
        return result;
    }
    
    public static String getFormatedResponse() {
        return (new OrderResultMessage()).toString ();
    }

}

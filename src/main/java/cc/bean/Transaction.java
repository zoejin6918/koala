package cc.bean;


import java.sql.Timestamp;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
/**
 * This is a bean class for transaction.
 * @author Zoe Zou
 */
@Entity
@Table(name = "Transaction")
@NamedQueries(
    { @NamedQuery(name = "Transaction.getMaxID", query = "Select max(t.id) as maxid from Transaction t"),
        @NamedQuery(name = "Transaction.findAll", query = "SELECT t FROM Transaction t order by t.id") })
public class Transaction
{
    @Id
    @Column(name = "transaction_id") 
    private Integer id;
    @Column(name = "transaction_time")
    private Timestamp time = new Timestamp (System.currentTimeMillis ());
    @Column(name = "transaction_price")
    private double price;
    @Column(name = "transaction_size")
    private int size;
    @Column(name = "tickerName")
    private String ticker;
    @Column(name = "transaction_action")
    private String action;

    public Transaction ()
    {

    }

    public Transaction (Integer id, Timestamp time, double price, int size,
        String ticker, String action)
    {
        super ();
        this.id = id;
        this.time = time;
        this.price = price;
        this.size = size;
        this.ticker = ticker;
        this.action = action;
    }


    public Integer getId ()
    {
        return id;
    }

    public void setId (Integer id)
    {
        this.id = id;
    }

    public Timestamp getTime ()
    {
        return time;
    }

    public void setTime (Timestamp time)
    {
        this.time = time;
    }

    public double getPrice ()
    {
        return price;
    }

    public void setPrice (double price)
    {
        this.price = price;
    }

    public int getSize ()
    {
        return size;
    }

    public void setSize (int size)
    {
        this.size = size;
    }

    public String getTicker ()
    {
        return ticker;
    }

    public void setTicker (String ticker)
    {
        this.ticker = ticker;
    }

    public String getAction ()
    {
        return action;
    }

    public void setAction (String action)
    {
        this.action = action;
    }

}

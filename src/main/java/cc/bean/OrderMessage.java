package cc.bean;

import java.util.Date;

import cc.enums.Company;

/**
 * This is an object class for order message.
 * @author Zoe Zou
 *
 */
public class OrderMessage
{
    
    private boolean buyOrSell;
    private int id;
    private double price;
    private int size;
    private Company stock;
    private Date whenAsDate;
    
    public OrderMessage (boolean buyOrSell, int id, double price, int size,
        Company stock, Date whenAsDate)
    {
        super ();
        this.buyOrSell = buyOrSell;
        this.id = id;
        this.price = price;
        this.size = size;
        this.stock = stock;
        this.whenAsDate = whenAsDate;
    }

    public boolean isBuyOrSell ()
    {
        return buyOrSell;
    }

    public void setBuyOrSell (boolean buyOrSell)
    {
        this.buyOrSell = buyOrSell;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public double getPrice ()
    {
        return price;
    }

    public void setPrice (double price)
    {
        this.price = price;
    }

    public int getSize ()
    {
        return size;
    }

    public void setSize (int size)
    {
        this.size = size;
    }

    public Company getStock ()
    {
        return stock;
    }

    public void setStock (Company stock)
    {
        this.stock = stock;
    }

    public Date getWhenAsDate ()
    {
        return whenAsDate;
    }

    public void setWhenAsDate (Date whenAsDate)
    {
        this.whenAsDate = whenAsDate;
    }

    @Override
    public String toString ()
    {
        String result = "<trade>\r\n" + 
            "  <buy>" + isBuyOrSell () + "</buy>\r\n" + 
            "  <id>" + getId() + "</id>\r\n" + 
            "  <price>" + getPrice () + "</price>\r\n" + 
            "  <size>" + getSize() + "</size>\r\n" + 
            "  <stock>" + getStock () + "</stock>\r\n" + 
            "  <whenAsDate>" + getWhenAsDate () + "</whenAsDate>\r\n" + 
            "</trade>";
        
        return result;
    }
    
}

package cc.bean;


import cc.enums.Action;

/**
 * This is a class to implement MACDStrategy.
 * @author Zoe Zou
 *
 */
public class MACDStrategy extends Strategy{
    private int flag;
    private int previousFlag;
    
    private MovingAverage longTerm;
    private MovingAverage shortTerm;
    
    public MACDStrategy(Stock stock, int length_long, int length_short) {
        super(stock);
        this.longTerm = new MovingAverage(length_long);
        this.shortTerm = new MovingAverage(length_short);
        this.flag = 0;
        this.previousFlag = 0;
    }
    
    public Action runTwoMAStrategy() {
        
        double shortAvg = shortTerm.getAvg();
        double longAvg = longTerm.getAvg();
        
        if (shortAvg != 0 && longAvg != 0) {
            if (flag == 0) {
                flag = shortAvg > longAvg ? 1 : -1;
            }
            else {
                flag =  shortAvg > longAvg ? 1 : -1;
                if (flag != previousFlag) {
                    this.action = flag > previousFlag ? Action.BUY : Action.SELL;
                    String strategy = "It is time to " + this.action + " at Price=" + stock.getPrice () ;
                    System.out.println(strategy);
                } else {
                    this.action = Action.NONE;
                }
                
            }
            previousFlag = flag;
        }
        return this.action;
    }
    
    public void newPriceArrive(double currentPrice) {
        getLongTermStrategy ().addNumber (currentPrice);
        getShortTermStrategy ().addNumber (currentPrice);
        getStock ().setPrice (currentPrice);
    }
    
    
    public double getLongTermAvg() {
        return longTerm.getAvg();
    }
    
    public double getShortTermAvg() {
        return shortTerm.getAvg();
    }

    public MovingAverage getLongTermStrategy ()
    {
        return longTerm;
    }

    public void setLongTermStrategy (MovingAverage longTerm)
    {
        this.longTerm = longTerm;
    }

    public MovingAverage getShortTermStrategy ()
    {
        return shortTerm;
    }

    public void setShortTermStrategy (MovingAverage shortTerm)
    {
        this.shortTerm = shortTerm;
    }

    
}

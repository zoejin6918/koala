package cc.bean;

import java.util.Date;

/**
 * This is an object class for Price information.
 * @author Zoe Zou
 *
 */
public class PriceInfo
{
    Date time;
    double open;
    double high;
    double low;
    double close;
    int volume;
    
    public PriceInfo() {
        
    }
    public PriceInfo (Date time, double open, double high, double low,
        double close, int volume)
    {
        super ();
        this.time = time;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
    }
    public Date getTime ()
    {
        return time;
    }
    public void setTime (Date time)
    {
        this.time = time;
    }
    public double getOpen ()
    {
        return open;
    }
    public void setOpen (double open)
    {
        this.open = open;
    }
    public double getHigh ()
    {
        return high;
    }
    public void setHigh (double high)
    {
        this.high = high;
    }
    public double getLow ()
    {
        return low;
    }
    public void setLow (double low)
    {
        this.low = low;
    }
    public double getClose ()
    {
        return close;
    }
    public void setClose (double close)
    {
        this.close = close;
    }
    public int getVolume ()
    {
        return volume;
    }
    public void setVolume (int volume)
    {
        this.volume = volume;
    }
    
    
}

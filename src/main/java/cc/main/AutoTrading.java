package cc.main;

import cc.bean.Stock;

import cc.bean.Strategy;
import cc.bean.Transaction;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import cc.bean.MACDStrategy;
import cc.bean.OrderMessage;
import cc.bean.OrderResultMessage;
import cc.bean.PriceInfo;
import cc.enums.Action;
import cc.enums.BrokerResponse;
import cc.enums.Company;
import cc.jms.TradingMessageHandler;
import cc.jpaservice.JpaTransactionService;
import cc.trading.PriceService;

import java.util.logging.Logger;

/**
 * This is the main model class for back-end.
 * 
 * @author Zoe Zou
 *
 */
public class AutoTrading
{
    private static final long DEFAULT_TIMEOUT = 15000;
    private static Company ticker;
    private static Strategy strategy;
    private static boolean start;

    // assumes the current class is called MyLogger
    private final static Logger LOGGER = Logger.getLogger(AutoTrading.class.getName());

    public static void startMACDStrategy(Company ticker, int longTerm, int shortTerm, double threshold) {
        LOGGER.info ("START MACD STRATEGY.");
        start = true;
        try
        {
            new AutoTrading ().runMACDStrategy (ticker, longTerm, shortTerm, threshold);
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public static void stopStrategy() {
        LOGGER.info ("STOP MACD STRATEGY.");
        start = false;
    }
    
    public void runMACDStrategy (Company ticker, int longTerm, int shortTerm, double threshold)
        throws InterruptedException
    {
        setTicker (ticker);
        strategy = createMACDStrategy (ticker, longTerm, shortTerm); // Used for test

        while (start || strategy.getStock ().getProfitOrLose () < threshold)
        { // Using for test: run for 30 secs.
            PriceInfo latestPrice = PriceService.getLatestPrice (ticker);
            if (latestPrice.getClose () != strategy.getStock ().getPrice ())
            { // New price is released in the market, run the strategy.
                LOGGER.info ("New price arrive: " + latestPrice.getClose ());

                ((MACDStrategy) strategy)
                    .newPriceArrive (latestPrice.getClose ()); // Add new price to short & long strategy data, update price of stock;
                Action strategyAction = ((MACDStrategy) strategy)
                    .runTwoMAStrategy (); // Run Strategy
                LOGGER.info (
                    "longTermAvg=" + ((MACDStrategy) strategy).getLongTermAvg ()
                        + ", shortTermAvg="
                        + ((MACDStrategy) strategy).getShortTermAvg ());

                if (strategyAction.equals (Action.BUY))
                { // BUY
                    sendOrder (latestPrice, strategyAction, ticker,
                        latestPrice.getVolume ());

                    LOGGER.info ("Result Message:"
                        + OrderResultMessage.getFormatedResponse ());
                    BrokerResponse response = OrderResultMessage.getResponse ();
                    if (!response.equals (BrokerResponse.REJECTED))
                    { // The order is filled or partially filled.
                        Transaction trans = new Transaction (
                            OrderResultMessage.getId (),
                            new Timestamp (
                                OrderResultMessage.getWhenAsDate ().getTime ()),
                            OrderResultMessage.getPrice (),
                            OrderResultMessage.getSize (),
                            OrderResultMessage.getStock ().toString (),
                            strategyAction.toString ());
                        strategy.getStock ().newTrade (
                            OrderResultMessage.getSize (),
                            OrderResultMessage.getPrice (), strategyAction);
                        LOGGER.info ("stockAvgCst=" + strategy.getStock ().getAvgCost () + " ,stockP&L=" + strategy.getStock ().getProfitOrLose ());
                        
                        try {
                            JpaTransactionService.addTransaction (trans);
                            LOGGER.info ("A BUY TRANSACTION IS FINISHED.");
                        } catch(Exception e) {
                            LOGGER.warning ("DATABASE IS NOT WORKING, TRANSACTION CANNOT BE FINISHED.");
                        }
                        
                    } else {
                        LOGGER.info ("ORDER IS REJECTED.");
                    }
                }
                else if (strategyAction.equals (Action.SELL))
                { // SELL: Can send order only if user has a bunch of shares already.
                    int userHoldShares = strategy.getStock ().getNumOfShares ();
                    if (userHoldShares != 0)
                    {
                        int sellSize = userHoldShares > latestPrice.getVolume ()
                            ? latestPrice.getVolume ()
                            : userHoldShares;
                        sendOrder (latestPrice, strategyAction, ticker,
                            sellSize);

                        LOGGER.info  ("Result Message:"
                            + OrderResultMessage.getFormatedResponse ());
                        BrokerResponse response = OrderResultMessage
                            .getResponse ();
                        if (!response.equals (BrokerResponse.REJECTED))
                        { // The order is filled or partially filled.
                            Transaction trans = new Transaction (
                                OrderResultMessage.getId (),
                                new Timestamp (OrderResultMessage
                                    .getWhenAsDate ().getTime ()),
                                OrderResultMessage.getPrice (),
                                OrderResultMessage.getSize (),
                                OrderResultMessage.getStock ().toString (),
                                strategyAction.toString ());
                            strategy.getStock ().newTrade (
                                OrderResultMessage.getSize (),
                                OrderResultMessage.getPrice (), strategyAction);
                            LOGGER.info  ("stockAvgCst=" + strategy.getStock ().getAvgCost () + " ,stockP&L=" + strategy.getStock ().getProfitOrLose ());
                            
                            
                            try {
                                JpaTransactionService.addTransaction (trans);
                                LOGGER.info ("A SELL TRANSACTION IS FINISHED.");
                            } catch(Exception e) {
                                LOGGER.warning ("DATABASE IS NOT WORKING, TRANSACTION CANNOT BE FINISHED.");
                            }
                        } else {
                            LOGGER.info ("ORDER IS REJECTED");
                        }
                    }
                    else
                    {
                        LOGGER.info  (
                            "User does not have any shares, cannot SELL.");
                    }
                }

            }

            Thread.sleep (1000);
        }
        LOGGER.info  ("Strategy loop is finished.");
    }

    public void sendOrder (PriceInfo latestPrice, Action action, Company ticker,
        int size)
        throws InterruptedException
    {
        boolean buyOrSell = action == Action.BUY
            ? true
            : false;
        int id = (int) latestPrice.getTime ().getTime () * -1;
        OrderMessage order = new OrderMessage (buyOrSell, id,
            latestPrice.getClose (), size, ticker, latestPrice.getTime ());

        LOGGER.info  ("Sending new Order: " + order.toString ());
        LOGGER.info ("SENDING NEW ORDER TO ORDERBROKER...");
        TradingMessageHandler tmh = new TradingMessageHandler ();
        tmh.run (order.toString ());
        //       OrderResultMessage orm = new OrderResultMessage ();  // static attributes in OrderResultMessage are set in MessageListener
        long currentTime = System.currentTimeMillis ();
        while (OrderResultMessage.getId () != order.getId ()
            && System.currentTimeMillis () < currentTime + DEFAULT_TIMEOUT)
        {
            Thread.sleep (1000);
        } // Waiting for MessageListener to receive message.

    }

    /**
     * Create a new MACD Strategy when user click 'START' button.
     * 
     * @param company
     * @param longTerm
     * @param shortTerm
     * @return
     */
    public MACDStrategy createMACDStrategy (Company company, int longTerm,
        int shortTerm)
    {
        Stock stock = new Stock (company);

        MACDStrategy rtStrategy = new MACDStrategy (stock, longTerm, shortTerm);

        ArrayList<PriceInfo> shortTermData = new ArrayList<PriceInfo> ();
        ArrayList<PriceInfo> longTermData = new ArrayList<PriceInfo> ();

        do
        {
            // Set short term strategy:
            shortTermData = PriceService.getTotalPriceInfo (company, shortTerm);
            for (PriceInfo p : shortTermData)
            {
                rtStrategy.getShortTermStrategy ().addNumber (p.getClose ());
            }

            // Set long term strategy:

            longTermData = PriceService.getTotalPriceInfo (company, longTerm);
            for (PriceInfo p : longTermData)
            {
                rtStrategy.getLongTermStrategy ().addNumber (p.getClose ());
            }

            PriceInfo latestPrice = PriceService.getLatestPrice (company);
            rtStrategy.getStock ().setPrice (latestPrice.getClose ());
        }
        while (!checkSameLatestPrice (
            shortTermData.get (shortTerm - 1).getClose (),
            longTermData.get (longTerm - 1).getClose (),
            rtStrategy.getStock ().getPrice ()));

        LOGGER.info ("NEW MACD STRATEGY IS CREATED.");
        rtStrategy.runTwoMAStrategy ();
        return rtStrategy;
    }

    private boolean checkSameLatestPrice (double lastPriceOfShort,
        double lastPriceOfLong, double priceOfStock)
    {
        return lastPriceOfLong == lastPriceOfShort
            && lastPriceOfShort == priceOfStock;
    }

    public static Company getTicker ()
    {
        return ticker;
    }

    public static void setTicker (Company ticker)
    {
        AutoTrading.ticker = ticker;
    }

    public static Strategy getStrategy ()
    {
        return strategy;
    }

    public static void setStrategy (Strategy strategy)
    {
        AutoTrading.strategy = strategy;
    }
    
    public static List<Transaction>  getTransactionHistory(int period) {
        try {
            LOGGER.info ("GET ALL TRANSACTION DATABASE.");
            return JpaTransactionService.getTransactionHistory(period);
            
        } catch (Exception e) {
            LOGGER.warning ("DATABASE IS NOT WORKING, CANNOT GET TRANSACTION HISTORY.");
            return new ArrayList<Transaction>();
    }
}

}

package cc.jms;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

/**
 * This is the message handler class.
 * @author Zoe Zou
 *
 */
public class TradingMessageHandler {
	
	private ApplicationContext context;
	private JmsTemplate jmsTemplate;
	private Destination destination;
	private Destination replyTo;
	
	public TradingMessageHandler() {
		context = new ClassPathXmlApplicationContext("spring-beans.xml");
		destination = (Destination) context.getBean("destination", Destination.class);
		replyTo = (Destination) context.getBean("replyTo", Destination.class);
		jmsTemplate = (JmsTemplate) context.getBean("messageSender");
	}
	
	public void run (String message) {
	    makeMessage(destination, message);
	    System.out.println("Message Sent to JMS Queue:- ");
	}

	private void makeMessage(final Destination destination, String msgContent) {
		jmsTemplate.send(destination, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {

			    TextMessage message = session.createTextMessage ();
			    message.setText (msgContent);
			    
				message.setJMSReplyTo(replyTo);
				message.setJMSCorrelationID("koala");
				return message;
			}
		});

	}
}
/*		Map map = new HashMap();
map.put("Name", "Vimal");
map.put("Age", new Integer(45));

jmsTemplate.convertAndSend("/dynamicqueues/myqueue", map,
		new MessagePostProcessor() {
			public Message postProcessMessage(Message message)
					throws JMSException {
				message.setIntProperty("ID", 9999);
				message.setJMSCorrelationID("123-99999");
				message.setJMSReplyTo(replyTo);
				return message;
			}
		});
}
*/
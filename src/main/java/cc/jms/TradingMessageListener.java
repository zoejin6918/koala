package cc.jms;


/**
 * 
 */
// for JMS Messaging
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import cc.bean.OrderResultMessage;

/**
 * This is message listener class.
 * @author Zoe Zou
 *
 */
public class TradingMessageListener implements MessageListener {
    
    public TradingMessageListener() {
        
    }

    @SuppressWarnings("unchecked")
    public void onMessage(Message m) {
        System.out.println( "In Listener");
        TextMessage message = (TextMessage) m;
        try {
            OrderResultMessage orm = new OrderResultMessage(message.getText ());
//            System.out.println (orm.getFormatedResponse ());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

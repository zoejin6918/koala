package cc.rest;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cc.bean.Transaction;
import cc.enums.Company;
import cc.jpaservice.JpaTransactionService;
import cc.main.AutoTrading;

/**
 * This is Rest Service class.
 * @author Lihua Shen
 *
 */
@RestController
@CrossOrigin(origins="http://localhost:4200")
public class TransactionController
{
    @RequestMapping(method=RequestMethod.GET, value="/transactions")   
    public List<Transaction> showTransactionHistory(@RequestParam int period) {
         return AutoTrading.getTransactionHistory(period);
        
    }
    
    @RequestMapping(method=RequestMethod.GET, value="/strat")
    public void startStrategy(@RequestParam String ticker, @RequestParam int longTerm, @RequestParam int shortTerm, @RequestParam String strategy, @RequestParam int threshold) {
        Company company = Company.valueOf (ticker);
        if (strategy.equals ("MACD")) {
            AutoTrading.startMACDStrategy (company, longTerm, shortTerm, ((double)threshold / 100));
        } else if (strategy.equals ("NONE")) {
            AutoTrading.stopStrategy();
        }
    }
    
    @RequestMapping(method=RequestMethod.GET, value="/pl")
    public double getProfitLoss() {
        return AutoTrading.getStrategy ().getStock ().getProfitOrLose ();
        
    }
}
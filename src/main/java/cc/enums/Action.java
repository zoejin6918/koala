package cc.enums;

/**
 * 
 * @author Zoe Zou
 *
 */
public enum Action
{
    BUY,
    SELL,
    NONE;

}

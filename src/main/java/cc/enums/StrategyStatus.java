package cc.enums;

/**
 * 
 * @author Zoe Zou
 *
 */
public enum StrategyStatus
{
    START,
    STOP,
    PAUSE,
    RESTART;
}

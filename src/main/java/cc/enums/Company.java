package cc.enums;

/**
 * 
 * @author Zoe Zou
 *
 */
public enum Company
{
    AAPL, BG, GOOG, REMX, RIO,
    HON, MSFT, NUAN, OLN, SIEGY,
    MRK,
    AA,
    BOM;
}

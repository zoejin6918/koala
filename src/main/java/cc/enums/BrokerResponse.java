package cc.enums;

/**
 * 
 * @author Zoe Zou
 *
 */
public enum BrokerResponse
{
    FILLED,
    REJECTED,
    PARTIALLY_FILLED;
}
